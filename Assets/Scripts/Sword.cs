﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using Mediapipe.HandTracking;
using System.Data;

public class Sword : MonoBehaviour
{
    //GameManager manager = new GameManager();
    public float strange;
    //private Rigidbody physic;
    //private Hand hand;

    private void Awake()
    {
        //physic = gameObject.GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        Hand hand = FindObjectOfType<Process>().current_hand;
        var landmarkAnchor = hand.GetLandmark(17);
        var landmarkForVector = hand.GetLandmark(9);
        if (landmarkAnchor.x < landmarkForVector.x || landmarkAnchor.y < landmarkForVector.y)
        {
            transform.position = new Vector3(landmarkAnchor.x, landmarkAnchor.y, landmarkAnchor.z - 0.1f);
            transform.LookAt(new Vector3(landmarkForVector.x, landmarkForVector.y, landmarkForVector.z - 0.1f));
        }
        else
        {
            transform.position = new Vector3(landmarkAnchor.x, landmarkAnchor.y, landmarkAnchor.z + 0.1f);
            transform.LookAt(new Vector3(landmarkForVector.x, landmarkForVector.y, landmarkForVector.z + 0.1f));
        }
        transform.Rotate(-90, 0, 0);
    }


    //spawning 
    public void SpawnDestroySword()
    {
        Instantiate(gameObject);
    }
}
