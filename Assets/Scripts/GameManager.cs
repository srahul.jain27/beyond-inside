﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField]public GameObject sword;
    [SerializeField] public GameObject mob;
    public GameObject playerObject;
    public Gesture gesture;
    [SerializeField]
    private LifeUI life = null;
    [NonSerialized]public Player player;
    [SerializeField]
    private GameObject start_button = null;
    [SerializeField]private Text level_text = null;


    void Start()
    {
        StartGame();
    }

    void SetHandPlayer()
    {
        player = playerObject.gameObject.GetComponent<Player>();
        if (player == null)
        {
            level_text.text = "Игрок не найден";
            return;
        }
    }

    public void StartGame()
    {
        SetHandPlayer();
        start_button.SetActive(false);
        DisplayLife();
        if (player != null) DisplayLevel();
    }


    private void DisplayLife()
    {
        int life_display = Convert.ToInt32(Math.Round(player.currentHealth));
        life.Display(life_display);
    }

    private void DisplayLevel()
    {
        level_text.text = Convert.ToString(player.level);
    }
}
